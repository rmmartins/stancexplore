#/usr/bin/env sh

# Alternatives to LibreOffice:
#   (http://stackoverflow.com/questions/10557360/convert-xlsx-to-csv-in-linux-command-line)
#   Gnumeric (ssconvert)
#   xlsx2csv (python)
#   csvkit (python)

for in in $@; do
    # LibreOffice is the best method so far, able to convert all spreadsheets
    # Even those with errors
    libreoffice --headless --convert-to csv "$in";
    in_csv=$(dirname $in)/$(basename $in .xlsx).csv  
    in_csv_utf8=$(dirname $in)/$(basename $in .xlsx).utf8.csv  
    iconv -f iso-8859-1 -t utf8 $in_csv >$in_csv_utf8 2>iso2utf8.log
    mv $in_csv_utf8 $in_csv
done
