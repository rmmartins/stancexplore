import re
import sys
from nltk.corpus import stopwords

# TODO external blacklist

def clean(str):
    str = re.sub("@.*?\s", '', str)
    str = re.sub("#.*?\s", '', str)
    str = re.sub("<URL>.*?</URL>", '', str)
    str = re.sub("<.*?>", '', str)
    str = re.sub("[^a-zA-Z_\' ]", '', str)
    str = str.lower().strip()
    # Stopwords removal
    str = str.split()
    sw = set(stopwords.words('english'))
    str = [word for word in str if word not in sw]
    # Assemble it back again (plus, without extra whitespace)
    return " ".join(str)

def eputs(str):
    sys.stderr.write(str + "\n")
    sys.stderr.flush()


# This comes straight from the data;
# must be updated if more misspellings appear.
misspellings = {}
# In these cases they must be added up to the final correct value
misspellings["Upplands Väsby"] = ["Upplands-Väsby"]
misspellings["Stockholm"] = ["Estocolmo", "Stoccolma"]
misspellings["Göteborg"] = ["Goteborg", "Gotenburg", "Gothenburg",
        "Gutemburgo", "Gøteborg"]
misspellings["Malmö"] = ["Malmo", "Malmø"]
misspellings["Uppsala"] = ["Upsala"]
#
def consolidate_city(city):
    for key in misspellings:
        for value in misspellings[key]:
            if city == value:
                return key
    return city


def column_dict(row, names, indices):
    r = {}
    for i in indices:
        value = row[i]
        if row[i] in ['true', 'false']:
            value = (value == 'true')
        elif row[i].isdigit():
            value = int(value)
        r[names[i]] = value
    return r

def hashtags(text):
    # Twitter screws this up; it thinks swedish chars are not chars
    text = re.sub("</?HT>", "", text)
    # But python knows better!
    return re.findall('#\w+', text)
