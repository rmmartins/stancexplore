# encoding: utf-8

import csv
import glob
import operator
from collections import defaultdict
from nltk.corpus import stopwords
import sys
import os
import json
import re
from pymongo import MongoClient
from sklearn.feature_extraction.text import CountVectorizer
import sklearn.manifold as skmf
import sklearn.metrics.pairwise as skmt
import numpy as np
from collections import defaultdict

import tools

# Current filters:
# * No tags
# * Words with more than 1 character
# * Words with more than 1 occurrence
# * Only the N most frequent

# Current transformations:
# * Only letters (all lowercase)
# * City consolidation

# TODO Hovering
# TODO Zoom
# TODO resize wordcloud
# TODO draw scale

N = 1000
blacklist = ['SpotifyNowPlay','slappervader']
output_prefix = 'may'

# MongoDB
client = MongoClient()
db = client.twitter

# TODO ignore tweets without user_id
tw_filter = {
    'country_code': 'SE',
    'lang': 'en',
    'text': { '$ne': '' },
    'user_id': { '$nin': blacklist },
    'classification': { '$exists': True }
}

tw_projection = {k:1 for k in
    ['_id', 'user_id', 'classification', 'city', 'date', 'text']
}

def hashtags():
    proj = {
        'text': 1
    }

    ht = defaultdict(int)
    for doc in db.tweets.find(tw_filter, proj):
        for s in tools.hashtags(doc['text']):
            ht[s] += 1
    ht = sorted(ht.items(), key=lambda x: x[1], reverse=True)
    for tag in ht:
        # print(tag[1], "\t", tag[0])
        print(json.dumps(tag, ensure_ascii=False))

def get_classification(full_classif):
    # Interpret classification results (as best as possible)
    classification = 'None'
    score = 0
    for k,v in full_classif[0].items():
        if k != 'sentence_text' and v[0] != 'no':
            tmp_score = 0
            if v[2][0][0] != 'no':
                tmp_score = float(v[2][0][1])
            else:
                tmp_score = float(v[2][1][1])
            if tmp_score > score:
                score = tmp_score
                classification = v[0]
    return classification

def full_dump(projection=tw_projection, include_text=False):
    tweets = db.tweets.find(tw_filter, projection)
    # print('Query done: ' + str(tweets.count()) + ' results.')
    new_tweets = []
    for tw in tweets:
        _tw = tw.copy()
        new_tweets.append(_tw)
        # City names are highly inconsistent
        _tw['city'] = tools.consolidate_city(tw['city'])
        # Stupid thing I did: saved some classifications as strings...! :(
        if isinstance(_tw['classification'], str):
            _tw['classification'] = json.loads(_tw['classification'])
        _tw['classification'] = get_classification(_tw['classification'])
        _tw['hashtags'] = tools.hashtags(_tw['text'])
        if not include_text:
            del _tw['text']
    return new_tweets

# ---------- DAYS ----------
def days():
    # days = db.tweets.aggregate([
    #     { "$match": tw_filter },
    #     { "$group": { "_id": "$date", "total": { "$sum": 1 } } }
    # ])

    days = defaultdict(int)
    for t in filtered_tweets:
        days[t['date']] += 1

    with open(output_prefix + ".days.json", "w") as f:
        json.dump(days, f, indent=2)


def users():
    tw_content = bag_of_words()
    # Grab users/tweets from DB, indexed by _id
    index = db.tweets.find(tw_filter, {'_id':1, 'user_id':1})
    index = {x['_id']: x['user_id'] for x in index}
    # Reduce words by user
    tf_by_user = defaultdict(lambda: {'text': '', 'tf': {}, 'count': 0})
    for tw in tw_content:
        user = tf_by_user[index[tw['_id']]]
        user['text'] += tw['clean_text'] + ' '
        user['count'] += 1
    # Turn it into an array and filter by amount of tweets
    tf_by_user = [{**v, **{'user_id':k}} for k,v in tf_by_user.items() if v['count'] > 100]
    print('Remaining users: {}'.format(len(tf_by_user)))
    # Do this just as if users were documents
    count_vect = CountVectorizer()
    tf = count_vect.fit_transform(x['text'] for x in tf_by_user).tocoo();
    # Dimensionality Reduction
    dr = reduce(tf.toarray(), 'tsne')
    # Map back to IDs
    for i,user in enumerate(tf_by_user):
        user['coords'] = dr[i].tolist()
    return tf_by_user

# Vaso asked me for ~100 tweets of each category. I added the users.
def vaso_dump():
    all_tweets = full_dump(projection=None)
    # print(','.join(all_tweets[0].keys()))
    class_count = defaultdict(int)
    user_ids = set()
    for tw in all_tweets:
        c = tw['classification']
        if class_count[c] < 200 and tw['user_id']:
            user_ids.add(tw['user_id'])
            # print(','.join(['"{}"'.format(v) for v in tw.values()]))
            class_count[c] += 1
    # Users
    users = db.users.find({ '_id': { '$in': list(user_ids) } })
    print(','.join(users[0].keys()))
    for user in users:
        print(','.join(['"{}"'.format(v) for v in user.values()]))


print(json.dumps(full_dump(include_text=True), indent=2, ensure_ascii=False))
# hashtags()
