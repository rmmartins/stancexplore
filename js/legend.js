//Stolen from: http://bl.ocks.org/nbremer/5cd07f2cb4ad202a9facfbd5d2bc842e

function addLegend(svg, type, colors, values) {
	//Needed for gradients
	var defs = svg.append("defs");

	//Calculate the gradient
	defs.append("linearGradient")
		.attr("id", "legend-gradient")
		.attr("x1", "0%").attr("y1", "0%")
		.attr("x2", "100%").attr("y2", "0%")
		.selectAll("stop")
		.data(colors)
		.enter().append("stop")
		.attr("offset", function(d,i) { return i/(colors.length-1); })
		.attr("stop-color", function(d) { return d; });

	///////////////////////////////////////////////////////////////////////////
	////////////////////////// Draw the legend ////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	var svgJq = $("#" + svg.attr("id")),
		width = svgJq.width(),
		height = svgJq.height();

	//Color Legend container	
	var legendsvg = svg.append("g")
		.attr("class", "legendWrapper")
	var legendJq = $(".legendWrapper");

	//Draw the Rectangle
	var legendWidth = width * 0.4,
		legendHeight = 10;	
	rect = legendsvg.append("rect")
		.attr("class", "legendRect")
		.attr("x", 0)
		.attr("y", 0)
		.attr("width", legendWidth)
		.attr("height", legendHeight)		
		.style("fill", "url(#legend-gradient)")
		.style("stroke-width", "1")
		.style("stroke", "rgb(0,0,0)");
		
	//Append title	
	var titleX = legendWidth / 2;
	var titleY = -5;
	legendsvg.append("text")
		.attr("class", "legendTitle")
		.attr("x", 0)
		.attr("y", 0)
		.attr("transform", "translate(" + titleX + ", " + titleY + ")")
		.text("Total Tweets (" + type + ")");

	//Set scale for x-axis
	var domain = [1, d3.max(values)];
	var xScale = d3.scale.log()
	if (type != "log") {
		xScale = d3.scale.linear();
	}	
	xScale.range([0, legendWidth])
		 .domain(domain);

	//Define x-axis
	var xAxis = d3.svg.axis()
		  .orient("bottom")		  		  
		  .scale(xScale);
	if (type == "log") {
		xAxis.ticks(3, "f")  //Set rough # of ticks
	} else {
		xAxis.ticks(5)  //Set rough # of ticks
	}


	//Set up X axis
	legendsvg.append("g")
		.attr("class", "axis")  //Assign "axis" class
		.attr("transform", "translate(0, " + (legendHeight / 2) + ")")
		.call(xAxis);

	// I still don't understand why I need these fine adjustments	
	var x = 10;
	var y = 20;
	legendsvg.attr("transform", "translate(" + x + ", " + y + ")");

	return legendsvg;
}