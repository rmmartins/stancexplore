// TODO this should be a class!!!
// TODO require a module for colormap/categories

var mainSelection, cities, _data, _colormap, citiesMap;

function mapChangeColor(categories) {
  classify(cities, _data, 'name', categories);

  let new_colormap = _colormap;
  if (categories) {
    if (categories.length == 1) {
      new_colormap = d3.scale.log()
         .domain([0.1, d3.max(cities.map(d => d.clusterCount))])
         .range(['#fff', _colormap(categories[0])]);
    } else if (categories.length == 2) {
      new_colormap = d3.scale.linear()
         .domain([-1, 0, 1])
         .range([_colormap(categories[0]), '#fff', _colormap(categories[1])]);
    }
  }

  mainSelection.style('fill', d => {
    let name = d.properties.KNNAMN;
    let classif = 0.01;
    if (citiesMap.has(name)) {
      if (categories && categories.length > 0) {
        classif = citiesMap.get(name).clusterCount;
      } else {
        classif = citiesMap.get(name).cluster;
      }
    }
    return new_colormap(classif);
  });
}

function draw(map, data, colormap) {
  _data = data;
  _colormap = colormap;

  // Yeah, I know, 'name', sue me
  cities = countByKey(data, 'name', false);
  // let counts = [...(cities.map(c => c.value))];

  // in-place assignment of categories (d => d.cluster)
  // classify(cities, data, 'name');

  // Helper (temporary)
  // citiesMap = cities.reduce((prev, curr) => {
  //   prev.set(curr.key, curr);
  //   return prev;
  // }, new Map());

  // Convert from TopoJson back to a GeoJson FeatureCollection:
  if (map.type == 'Topology') {
    // The 'subunits' part is not standard; it's just me.
    topomap = map;
    map = topojson.feature(map, map.objects.subunits);
  }

  for (f of map.features) {
    // Using 0.1 because log scales can't handle 0
    f.value = cities.get(f.properties.KNNAMN) || 0.1;
  }

  colormap = d3.scale.log()
     .domain([0.1, d3.max(map.features.map(d => d.value))])
     .range(colorbrewer.BuPu['3']);

  //http://stackoverflow.com/questions/14492284/center-a-map-in-d3-given-a-geojson-object

  // Answer #2: Create a unit projection.
  let projection = d3.geo.mercator() // my choice
    .scale(1)
    .translate([0, 0]);

  // Answers #1 and #2: create the path
  let path = d3.geo.path().projection(projection);

  // Compute the bounds of a feature of interest, then derive scale & translate.
  let b = path.bounds(map),
    s = .95 / Math.max((b[1][0] - b[0][0]) / width, (b[1][1] - b[0][1]) / height),
    t = [(width - s * (b[1][0] + b[0][0])) / 2, (height - s * (b[1][1] + b[0][1])) / 2];

  // Answer #2: Update the projection to use computed scale & translate.
  projection
    .scale(s)
    .translate(t);

  // Colors:
  // --------------
  // let max_area = d3.max( counties, function(d) { return d['properties']['CENSUSAREA'] });

  // let colorRange = ['#ece7f2','#a6bddb','#2b8cbe'];

  // changeScale(counts, colorRange);

  let used_cities = [];

  let tooltip = d3.select('body').append('div')
            .attr('class', 'hidden tooltip');

  mainSelection = svg.selectAll('.subunit')
      .data(map.features)
    .enter().append('path')
      .attr("class", "subunit");

  mainSelection
      .style('fill', function(d) {
        return colormap(d.value);
      })
      .attr('d', path)
      .on('mousemove', function(d) {
        let mouse = d3.mouse(document.body);
        tooltip.classed('hidden', false)
            .attr('style', 'left:' + mouse[0] + 'px;' +
                'top:' + (mouse[1] - 20) + 'px')
            .html(d.properties.KNNAMN + ': ' + d.value);
      })
      .on('mouseout', function() {
          tooltip.classed('hidden', true);
      });

  let scale = d3.scale.log()
     .domain([0.1, d3.max(map.features.map(d => d.value))])
     .range([0, 20]);

  mainSelection.filter(d => path.area(d) < 50).each(d => {
    svg.append('circle')
        .attr('class', 'centroid')
        .attr('cx', path.centroid(d)[0])
        .attr('cy', path.centroid(d)[1])
        .attr('r', function(i) {
          let name = d.properties.KNNAMN;
          let v = d.value;
          // this.ratio = v / path.area(d);
          // if (this.ratio > 100) {
          //   console.log(`${name}: ${this.ratio}`);
          // } else {
          //   v = 0.1;
          // }
          return scale(v);
        });
  });

  // Inner boundaries (between regions):
  // -----------------------------------
  // svg.append('path')
  //   .datum(topojson.mesh(topomap, topomap.objects.subunits, function(a, b) { return a !== b; }))
  //   .attr('d', path)
  //   .attr('class', 'inner-boundary');

  // External outline of the country:
  // --------------------------------
  svg.append('path')
    .datum(topojson.mesh(topomap, topomap.objects.subunits, function(a, b) { return a == b; }))
    .attr('d', path)
    .attr('class', 'outer-boundary');

  // FIXME 'sweden_cities.topo.json' doesn't have 'places'

  // addLegend(svg, scaleType, colorRange, counts)
  //     .on('click', function(d){
  //         if (scaleType == 'log') {
  //           scaleType = 'linear';
  //         } else {
  //           scaleType = 'log';
  //         }
  //         draw(map, data);
  //     });

}
