// All these should be variables in a closure...
let values, parents, countiesLayer, citiesLayer;
let highlightStyleCache = {}, styleCache = new Map();
let highlight, overlay, mapContainer, countiesCounts;
let swedenCities, swedenCounties, citiesCat, countiesCat;
let citiesScale, countiesScale, legendScale, yAxis;
let activeScale, scaleName = "county";
let legendSvg, selectedFeature = null;

// let brewer = colorbrewer.Blues[9];
let brewer = colorbrewer.YlOrRd[9];
let rangeSteps = brewer.length - 1;

// Pie
let pieGroup, pie, path;

function map_ol(data, colormap) {
  // ---------- Gathering Data ----------

  citiesCat = categoriesByKey(data, 'city');
  countiesCat = new Map();

  swedenCounties = new ol.source.Vector({
    url: 'data/sweden-counties.geojson',
    format: new ol.format.GeoJSON()
  });

  countiesScale = d3.scale.log()
      .range(brewer);

  activeScale = countiesScale;

  swedenCities = new ol.source.Vector({
    url: 'data/sweden_cities.topo.json',
    format: new ol.format.TopoJSON()
  });

  citiesScale = d3.scale.log()
      .range(brewer);

  overlay = d3.select('#overlay');
  mapContainer = d3.select('#map');
  legendSvg = d3.select('#map-legend');

  legendSvg.on('click', function(d) {
    if (activeScale != citiesScale) {
      citiesLayer.setVisible(true);
      countiesLayer.setVisible(false);
      activeScale = citiesScale;
      scaleName = "city";      
    } else {
      citiesLayer.setVisible(false);
      countiesLayer.setVisible(true);
      activeScale = countiesScale;
      scaleName = "county";
    }
    updateLegend(false);
  });

  let cw = mapContainer.node().getBoundingClientRect().width;
  let ch = mapContainer.node().getBoundingClientRect().height;
  let w = overlay.node().getBoundingClientRect().width;
  let h = overlay.node().getBoundingClientRect().height;
  overlay.style('top', `${ch-h-10}px`);
  overlay.style('left', `${cw-w-10}px`);

  pieGroup = overlay.select('svg')
      .attr('width', w)
      .attr('height', h - 30)
      .style('top', '25px')
      .style('left', '5px')
    .append('g')
      .attr('transform', `translate(${(w-10)/2},55)`);

  pieGroup.append('text');

  let radius = (w - 20) / 2;
  pie = d3.layout.pie()
      .sort(d3.ascending)
      .value(d => d[1]);
  path = d3.svg.arc()
    .outerRadius(radius)
    .innerRadius(20);

  // ---------- Layers ----------

  countiesLayer = new ol.layer.Vector({
    source: swedenCounties
  });
  countiesLayer.setZIndex(-1);

  citiesLayer = new ol.layer.Vector({
    source: swedenCities
  });

  let tileSource = new ol.source.OSM();

  let tileLayer = new ol.layer.Tile({
    source: tileSource
  });
  tileLayer.setOpacity(0.25);
  tileLayer.setZIndex(-99);

  var button = document.createElement('button');
  button.className = 'back';
  button.innerHTML = '&ndash;';
  button.addEventListener('click', () => zoomOut(), false);
  var element = document.createElement('div');
  element.className = 'rotate-north ol-unselectable ol-control';
  element.appendChild(button);
  let control = new ol.control.Control({element: element});

  var map = new ol.Map({
    layers: [
      tileLayer,
      citiesLayer,
      countiesLayer,
    ],
    target: 'map',
    view: new ol.View({
      center: [0, 0],
      zoom: 1
    }),
    controls: [control],
    interactions: [new ol.interaction.DragPan()]
  });

  var featureOverlay = new ol.layer.Vector({
    source: new ol.source.Vector(),
    map: map,
    style: function(feature, resolution) {
      var text = feature.get('name') || feature.get('KNNAMN');
      if (!highlightStyleCache[text]) {
        // Returning an array of styles means each will be applied over the
        // previous one (for each feature)
        highlightStyleCache[text] = [
          // Style for the polygon
          new ol.style.Style({
            stroke: new ol.style.Stroke({
              color: '#666',
              width: 3
            })
          }),
          // Style for the text
          new ol.style.Style({
            text: new ol.style.Text({
              font: '12px Calibri,sans-serif',
              text: text,
              fill: new ol.style.Fill({
                color: '#000'
              }),
              stroke: new ol.style.Stroke({
                color: '#fff',
                width: 3
              })
            }),
            geometry: function(feature) {
              let ext = feature.getGeometry().getExtent();
              return new ol.geom.Point(ol.extent.getCenter(ext));
            }
          })
        ];
      }
      return highlightStyleCache[text];
    }
  });

  var displayFeatureInfo = function(pixel) {
    var feature = map.forEachFeatureAtPixel(pixel, function(feature) {
      return feature;
    });
    if (feature !== highlight) {
      if (highlight) {
        featureOverlay.getSource().removeFeature(highlight);
      }
      if (feature) {
        featureOverlay.getSource().addFeature(feature);
        // Some counties and cities have the same name (e.g. Stockholm), so it's
        // important to know which we're dealing with
        let name, cats, counts;
        if (swedenCounties.getFeatures().includes(feature)) {
          name = feature.get('name');
          cats = countiesCat.get(name);
          counts = countiesCounts.get(name);
        } else {
          name = feature.get('KNNAMN');
          cats = citiesCat.get(name) || new Map([['None', 1]]);
          counts = values.get(name) || 0;
        }

        overlay.select('#name').text(filter(name));

        // Update the pie
        let arcs = pieGroup.selectAll(".arc")
            .data(pie([...cats]), d => d.data[0]);
        arcs.exit().remove();
        arcs.enter().append("g")
            .attr("class", "arc")
          .append('path');
        arcs.select('path')
            .attr('d', path)
            .attr('fill', d => colormap(d.data[0]));

        pieGroup.select('text').text(counts);

        // Make the pie visible
        overlay.style('visibility', 'visible');
      } else {
        overlay.style('visibility', 'hidden');
      }
      highlight = feature;
    }
  };

  map.on('pointermove', function(evt) {
    if (evt.dragging || selectedFeature) {
      return;
    }
    var pixel = map.getEventPixel(evt.originalEvent);
    displayFeatureInfo(pixel);
  });

  map.on("dblclick", function(e) {
    map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
      var extent = feature.getGeometry().getExtent();
      map.getView().fit(extent, {duration:500});      
    })
  });

  map.on("singleclick", function(e) {
    if (e.originalEvent.metaKey) {
      selectedFeature = null;
      cityDim.filterAll();
    } 
    else if (selectedFeature == null) {
      map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
        selectedFeature = feature;
      })
      // If the user did not click outside any feature...
      if (selectedFeature) {
        if (parents[selectedFeature.get('KNNAMN')]) {
          cityDim.filter(selectedFeature.get('KNNAMN'));
        }
        // County: select all cities
        else {
          let selectedCities = [];
          for (let city of Object.keys(parents)) {
            if (parents[city] == selectedFeature.get('name')) {
              selectedCities.push(city);
            }          
          }
          cityDim.filter(d => selectedCities.includes(d));
        }
      }
    }
  });

  function zoomOut(duration = 500) {
    let extent = ol.extent.createEmpty();
    for (f of swedenCounties.getFeatures()) {
      ol.extent.extend(extent, f.getGeometry().getExtent());
    }
    map.getView().fit(extent, {
      constrainResolution: false,
      padding: [10,100,10,10],
      duration: duration
    });
  }

  let initialZoom = true;

  map.on('postcompose', function(evt) {
    if (!parents && swedenCities.getFeatures().length > 0) {
      // ---------- Legend ----------
      drawLegend(legendSvg);
      // This had to be done here, on 'postcompose':
      parents = getParentsOf(swedenCities, swedenCounties);
      updateValues();
      // count categories for counties
      citiesCat.forEach((cityCats, city, map) => {
        let county = parents[city];
        cityCats.forEach((count, cat, map2) => {
          let countyCats = countiesCat.get(county) || new Map();
          let prevCount = countyCats.get(cat) || 0;
          countyCats.set(cat, prevCount + 1);
          countiesCat.set(county, countyCats);
        });
      });
      if (initialZoom && swedenCounties.getFeatures().length > 0) {
        initialZoom = false;
        zoomOut(0);
        // Initial setup
        citiesLayer.setVisible(false);
        updateLegend(false);      
      }
    }
  });

  tileLayer.on('postcompose', function(evt) {
    var ctx = evt.context;
    if (ctx) {
      let canvas = ctx.canvas, w = canvas.width, h = canvas.height;
      let imgd = ctx.getImageData(0, 0, w, h);
      var pix = imgd.data;
      for (var i = 0, n = pix.length; i < n; i += 4) {
        pix[i] = pix[i + 1] = pix[i + 2] = (3 * pix[i] + 4 * pix[i + 1] + pix[i + 2]) / 8;
      }
      ctx.putImageData(imgd, 0, 0);
    }
  });
}

function getParentsOf(children, parents) {
  let h = {};
  for (cf of children.getFeatures()) {
    let cn = cf.get('KNNAMN');
    let cg = cf.getGeometry();
    let c = {};
    if (cg instanceof ol.geom.Polygon)
      c = cg.getInteriorPoint().getCoordinates();
    else if (cg instanceof ol.geom.MultiPolygon)
      // This ensures that the point is inside a polygon
      c = cg.getInteriorPoints().getPoint(0).getCoordinates();
    else
      // Last resort; hopefully will not be used
      c = ol.extent.getCenter(cg.getExtent());
    h[cn] = parents.getFeaturesAtCoordinate(c)[0].get('name');
  }
  return h;
}

// This function will be called when the vectors are actually drawn
function updateValues() {
  // Recompute city counts with filtering, reducing array into an easier to handle object
  values = cityDim.group().all().reduce((acc, cur) => {
    return acc.set(cur.key, cur.value);
  }, new Map());

  citiesCat = categoriesByKey(cityDim.top(Infinity), 'city');

  citiesScale.domain(splitLogDomain(d3.extent([...values].map(d => d[1])), rangeSteps));

  // Recompute county counts (from city counts)
  countiesCounts = new Map();
  countiesCat = new Map();
  for (let city of Object.keys(parents)) {
    let p = parents[city];
    // Update count
    let count = countiesCounts.get(p) || 0;
    countiesCounts.set(p, count + (values.get(city) || 0));
    // Update stances
    if (citiesCat.get(city)) {
      let countyCats = countiesCat.get(p) || new Map();
      for (let cat of [...citiesCat.get(city)]) {
        let prevCount = countyCats.get(cat[0]) || 0;
        countyCats.set(cat[0], prevCount + cat[1]);
      }
      countiesCat.set(p, countyCats);
    }
  }
  
  let domain = d3.extent([...countiesCounts].map(d => d[1]));
  // console.log(domain);
  let ticks = splitLogDomain(domain, rangeSteps);
  countiesScale.domain(ticks);
  // console.log(ticks);

  countiesLayer.setStyle((feature, resolution) => {
    let name = feature.get('name');
    return new ol.style.Style({
      stroke: new ol.style.Stroke({
        color: '#fff',
        width: 2
      }),
      fill: new ol.style.Fill({
        color: countiesScale(countiesCounts.get(name) + 1)
      })
    });
  });

  citiesLayer.setStyle((feature, resolution) => {
    let name = feature.get('KNNAMN');
    return new ol.style.Style({
      stroke: new ol.style.Stroke({
        color: '#fff',
        width: 1
      }),
      fill: new ol.style.Fill({
        color: citiesScale(values.get(name) + 1)
      })
    });
  });

  if (selectedFeature) {
    let cats = new Map();
    if (swedenCounties.getFeatures().includes(selectedFeature)) {
      pieGroup.select('text').text(countiesCounts.get(selectedFeature.get('name')));
      cats = countiesCat.get(selectedFeature.get('name'));
    } else {
      pieGroup.select('text').text(values.get(selectedFeature.get('KNNAMN')));
      // Can be improved:
      cats = [...citiesCat][0][1];
    }
    // Update the pie
    pieGroup.selectAll(".arc").remove();
    pieGroup.selectAll(".arc")
        .data(pie([...cats]), d => d.data[0])
      .enter().append("g")
        .attr("class", "arc")
      .append('path')
        .attr('d', path)
        .attr('fill', d => colormap(d.data[0]));
  }
}

function drawLegend(svg) {
  let h = svg.node().getBoundingClientRect().height;
  let m = 10, w = 20;

  let g = svg.append("g")
      .attr("class", "y axis")
      .attr("transform", `translate(${45},${m})`);

  // Needed for gradients
	var defs = svg.append("defs");

	// Calculate the gradient
	defs.append("linearGradient")
  		.attr("id", "legend-gradient")
  		.attr("x1", "0%").attr("y1", "0%")
  		.attr("x2", "0%").attr("y2", "100%")
		.selectAll("stop")
  		.data(brewer)
		.enter().append("stop")
  		.attr("offset", function(d,i) { return i/(brewer.length-1); })
  		.attr("stop-color", function(d) { return d; });

  g.append('rect')    
    .attr('width', w)
    .attr('height', h - 2 * m)
    .attr('fill', 'url(#legend-gradient)');

  g.append('text')
    .attr('id', 'subtitle')
    .attr('transform', 'translate(30,80), rotate(90)')
    .text('Total tweets (by county)');

  yAxis = d3.svg.axis()    
    .orient("left")
    .tickFormat(d => Math.round(d));
}

function splitLogDomain(domain, steps) {
  let logDomain = domain.map(d => Math.log10(d));
  if (!isFinite(logDomain[0])) {
    logDomain[0] = 0;
  }
  return d3.range(logDomain[0], logDomain[1]+0.1,
    (logDomain[1]-logDomain[0])/steps).map(d => Math.pow(10,d))
    .map(d => Math.round(d))
}

function updateLegend(blink=true) {
  let h = legendSvg.node().getBoundingClientRect().height;
  let m = 10, w = 20;
  
  // I chose to copy to make sure that the legends have the same type
  // IMPORTANT! The scale's domain must be a d3.extent, or else the tick values
  // don't work properly!
  legendScale = activeScale.copy()
      .domain(d3.extent(activeScale.domain()))
      .range([0, h - 2 * m]);

  // IMPORTANT: Don't mess with the domain itself to get better tick values, or
  // the scale may become wrong!
  let tickValues = splitLogDomain(d3.extent(activeScale.domain()), 4);

  yAxis
      .scale(legendScale)
      .tickValues(tickValues);

  legendSvg.select('.y.axis').call(yAxis);

  legendSvg.select('#subtitle').text('Total tweets by ' + scaleName + ' (log)');

  if (blink) {
    legendSvg
        .transition()
        .duration(150)
        .attr('opacity', 0)
        .transition()
        .attr('opacity', 1)
        .transition()
        .attr('opacity', 0)
        .transition()
        .attr('opacity', 1);
  }
}

// There is some serious technical debt here that I must address at some point!
function updateMap() {
  updateValues();
  updateLegend();
}

let cityNameFilter = {
  Västerbotten: 'Väster- botten',
  Västernorrland: 'Väster- norrland',
  Västmanland: 'Västman- land',
  Södermanland: 'Söderman- land',
  Östergötland: 'Östergöt- land',
  Örnsköldsvik: 'Örnskölds- vik',
  Simrishamn: 'Simris- hamn',
  Staffanstorp: 'Staffans- torp',
  Kristianstad: 'Kristian- stad',
  Hässleholm: 'Hässle- holm',
  Helsingborg: 'Helsing- borg',
  Kungsbacka: 'Kungs- backa',
  Oskarshamn: 'Oskars- hamn',
  Mörbylånga: 'Mörby- långa',
  Valdemarsvik: 'Valdemars- vik',
  Söderköping: 'Söder- koping',
  Katrineholm: 'Katrine- holm',
  Kristinehamn: 'Kristine- hamn',
  Stenungsund: 'Stenung- sund'
}

function filter(name) {
  if (cityNameFilter[name])
    return cityNameFilter[name];
  return name;
}