d3.json('json/words.json', function(error, data) {

  let words = new Map();
  data.forEach(d => Object.keys(d.tf).forEach(w => {
    let prevCount = words.get(w) || 0;
    words.set(w, prevCount + d.tf[w]);
  }));
  words = [...words]
      .map(k => { return {text: k[0], size: k[1]}; })
      .sort((a,b) => b.size - a.size)
      .slice(0,100);

  var wc = d3.wordcloud()
      .size([100, 100])
      .words(words)
      .start();

  let scale = genColormap(words.map(d => d.size), colorbrewer.Greys[3]);

  d3.selectAll("#wordcloud svg text")
      .style("fill", d => scale(d.size));

});
