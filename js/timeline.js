// ---------- Draw Timeline ----------

let timelineData, graph, dates, categDim2;

function timeline() {

  let palette = new Rickshaw.Color.Palette();

  dates = dateDim.group().all();
  categDim2 = cfData.dimension(d => d.classification);

  let width = 500, height = 170;

  // Probably the least readable piece of code I've ever written
  timelineData = categDim2.group().reduce((a, c) => {
    a[c.date] += 1;
    return a;
  }, (a, c) => {
    a[c.date] -= 1;
    return a;
  }, () => {
    return dates.reduce((a, c) => {
      a[c.key] = 0;
      return a;
    }, {});
  }).all().map(c => {
    return {
      name: c.key,
      data: Object.keys(c.value).map(k => ({
        x: +k.slice(-2),
        y: c.value[k]
      })),
      color: colormap(c.key)
    }
  });

  graph = new Rickshaw.Graph( {
    element: document.querySelector("#chart"),
    // width: width,
    // height: height,
    renderer: 'area',
    stroke: true,
    series: timelineData
  } );

  var x_axis = new Rickshaw.Graph.Axis.X( { 
    graph: graph,
    tickValues: d3.range(dates.length + 1),
    tickFormat: x => {
      if (x == 1) return '1st';
      if (x == 2) return '2nd';
      if (x == 3) return '3rd';
      return `${Math.floor(x)}th`;
    }
  });

  var y_axis = new Rickshaw.Graph.Axis.Y( {
        graph: graph,
        orientation: 'right',
        tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
        element: document.getElementById('y_axis'),
        pixelsPerTick: 30,
  } );

  var slider = new Rickshaw.Graph.RangeSlider.Preview({
    graph: graph,
    element: document.querySelector('#slider')
  });

  // d3.select('#slider').on('dblclick', function(d) {
  //   var dateRange = d3.extent(
  //     graph.series.active()[0].data
  //       .filter(d => graph._slice(d))
  //       // The numbers are initially 1-indexed
  //       .map(d => d.x - 1));

  //   // Crossfilter does not include the end of the interval
  //   if (dateRange[1] < dates.length - 1) dateRange[1]++;

  //   // There is still an unsolved bug here, when the end of the interval is the last index.
  //   // i don't know how to fix it.
  //   dateRange = dateRange.map(d => dates[d].key);
  //   console.log(dateRange);
  //   dateDim.filter(dateRange);
  // });

  graph.render();

  d3.select('#timeline-heading')
    .append('text')
      .attr('transform', 'translate(20 150) rotate(270)')
      .text('May 2016');
}

function updateTimeline() {
  // Probably the least readable piece of code I've ever written
  let tmp = categDim2.group().reduce((a, c) => {
    a[c.date] += 1;
    return a;
  }, (a, c) => {
    a[c.date] -= 1;
    return a;
  }, () => {
    return dates.reduce((a, c) => {
      a[c.key] = 0;
      return a;
    }, {});
  }).all().map(c => {
    return {
      name: c.key,
      data: Object.keys(c.value).map(k => ({
        x: +k.slice(-2),
        y: c.value[k]
      })),
      color: colormap(c.key)
    }
  });

  for (let i = 0; i < tmp.length; ++i) {
    timelineData[i] = tmp[i];
  }

  graph.update();
}