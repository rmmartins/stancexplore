
function hexbin(parentId, data, selected = []) {
    let circles = false;

	// Reduce data per coordinates...
	data = Object.keys(data).reduce((map, k) => {
		let coords = `${data[k].x} ${data[k].y}`;
		let item = map[coords] || [0];
		item[0] += data[k].count;
		item.push(k);
		map[coords] = item;
		return map;
	}, {});
	// ...then map into an array for D3...
	data = Object.keys(data).map(k => {
		let coords = k.split(' ');
		return {
			x: +coords[0],
			y: +coords[1],
			count: data[k][0],
			tags: data[k].slice(1)
		}
	});

    let htIndex = data.reduce((map, h) => {
        for (let tag of h.tags) {
            map[tag] = h;
            return map;
        }
    }, {});

	let width = 310
		height = 250;

	let svg = d3.select(parentId).append('svg')
		.attr('width', '100%')
		.attr('height', 225);

    d3.select(parentId)
      .on('click', d => {
        if (d3.event.metaKey || d3.event.ctrlKey) {
          // clear selection
          while (selected.length > 0) selected.splice(0, 1);          
        }

        if (selected.length == 0) {
            htDim.filterAll();
        } else {
            htDim.filterFunction(function(tag) {
                return selected.includes(tag);
            });
        }

        draw();
      });

    let legend = d3.select(parentId).append('div')
        .attr('id', 'legend');

	let xBand = d3.max(data, d => d.x) + 2;
	let yBand = d3.max(data, d => d.y) + 1;

	let x = d3.scale.ordinal()
		.domain(d3.range(xBand))
		.rangeRoundPoints([0, width], 1.0);

	let y = d3.scale.ordinal()
		.domain(d3.range(yBand))
		.rangeRoundPoints([0, height * 0.9], 1.0);

	let rMax = Math.max(x(1) - x(0), y(1) - y(0)) / 2;

	let size = d3.scale.linear()
		.domain(d3.extent(data, d => d.count))
		.range([5, rMax]);

	let g = svg.selectAll('.hex')
			.data(data)
		.enter().append('g')
			.attr('class', 'hex');

    g.each(d => { d.count = 0; d.stances = {}; });

    for (let tweet of idDim.top(Infinity)) {
        if (tweet.classification != 'None')
            for (let tag of tweet.hashtags) {
                if (htIndex[tag]) {
                    let hex = htIndex[tag];
                    let cur = hex.stances[tweet.classification] || 0;
                    hex.stances[tweet.classification] = cur + 1;                    
                }
            }
    }

    // Colors are fixed
    let fill = data.map(d => {
        let stance = 'None', score = 0;
        if (d.stances) {
            for (let k of Object.keys(d.stances)) {
                if (d.stances[k] > score) {
                    score = d.stances[k];
                    stance = k;
                }
            }
        }
        return colormap(stance);
    });

    function draw() {
        let hex = svg.selectAll('.hex');

        let data = htDim.group().top(100);

        hex.each(d => { d.count = 0; });
        for (let tag of data) {
            if (htIndex[tag.key])                
                htIndex[tag.key].count += tag.value;
        }

        let ext = d3.extent(hex.data(), d => d.count);

        size.domain(ext);

        hex.select('path').remove();
        hex.select('circle').remove();

        let items;
        if (circles) {
            items = hex.append('circle')                
                .attr('cx', d => x(d.x) + (d.y % 2) * rMax)
                .attr('cy', d => y(d.y))
                .attr('r', d => size(d.count));
        } else {
            items = hex.append('path')                
                .attr('d', d => {
                    let cx = x(d.x) + (d.y % 2) * rMax,
                        cy = y(d.y),
                        r = size(d.count),
                        hW = r * Math.sqrt(3);                      
                        p = [
                            [cx, cy - r],
                            [cx + hW / 2, cy - r / 2],
                            [cx + hW / 2, cy + r / 2],
                            [cx, cy + r],
                            [cx - hW / 2, cy + r / 2],
                            [cx - hW / 2, cy - r / 2],
                            [cx, cy - r]
                        ];
                    return d3.svg.line()(p);
                })
                
        }

        items
            .attr('fill', (d,i) => fill[i])
            .attr('class', function(d) {
                if (selected.length > 0) {
                    let count = 0;
                    for (let tag of d.tags) {
                        if (selected.includes(tag)) {
                            count++;
                        }
                    }
                    if (count == d.tags.length) {
                        return 'selected-full';
                    } else if (count > 0) {
                        return 'selected-part';
                    } else {
                        return 'not-selected';
                    }
                }
                return '';                
            })            
            .on('mouseenter', d => {
                legend.text(d.tags.join(", "));
            })
            .on('mouseout', d => {
                legend.text(selected.join(", "));
            })
            .on('click', function(d) {
                // command or ctrl+click clears selection (by propagation to parent node)
                if (d3.event.metaKey || d3.event.ctrlKey)
                    return;
                
                // Normal+click:    exclusive selection
                // Shift+click:     add to selection
                if (d3.event.shiftKey) {
                    if (d3.select(this).classed('selected-full'))
                        for (let tag of d.tags)
                            selected.splice(selected.indexOf(tag), 1)
                    else
                        for (let tag of d.tags)
                            selected.push(tag);
                } else {
                    while (selected.length > 0) selected.splice(0, 1);
                    for (let ht of d.tags)
                        selected.push(ht);
                }
            });

        legend.text(selected.join(", "));
    }

    return draw;
}
