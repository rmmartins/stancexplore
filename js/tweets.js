
let server = 'http://localhost:27080',
    db = 'twitter',
    col = 'tweets',
    fields = '["text","classification"]';

function tweets() {
  let selected = null, tw_total = 100, join;

  // Recompute IDs considering all the filters
  let ids = idDim.top(Infinity).map(d => d._id);

  // Update heading
  d3.select('#total-tweets').html(`Total: ${ids.length}`);

  ids = ids.slice(0, tw_total);

  // Get data from server
  let criteria = `{"_id":{"$in":${JSON.stringify(ids)}}}`;
  let url = `${server}/${db}/${col}/_find?criteria=${criteria}&fields=${fields}&batch_size=${tw_total}`;

  d3.json(url, function(error, data) {
    // console.log(data);
    // return;

    // Map data into an index, with text and classification
    data = data.results.reduce((acc, cur) => { 
      acc[cur._id] = {
        text: cur.text
          .replace(/<AT>.*?<\/AT>/g, '<i>@User</i>')
          .replace(/<\/?HT>/g, '')
          .replace(/(#[\wäöåuÄÅÖ]+)/g, '<b>$1</b>')
          .replace(/<URL>(.*?)<\/URL>/g, '[...]')
          .replace(/fuck/ig, '****')
          .replace(/shit/ig, '****'),
        classification: getClassification(cur.classification)
      };
      // console.log(acc[cur._id].classification);
      return acc;
    }, {});

    // Remove all old rows
    d3.select('#tweets > tbody').selectAll('tr').remove();

    // IDs sorted in descending order by classification score
    ids.sort((a,b) => d3.descending(data[a].classification.score, data[b].classification.score));

    // Usual D3 join
    join = d3.select('#tweets > tbody').selectAll('tr')
        .data(ids)
      .enter().append('tr')
        .each(function(d) {
          let row = d3.select(this),
              t = data[d],
              h = 30;
          row.append('td')
              .attr('class', 'content')
              .html(t.text);          
          let bar = row.append('td')
            .append('svg')
              .attr('width', 15)
              .attr('height', h);
            
          bar.append('rect')
              .attr('x', 0)
              .attr('y', d => Math.min(1.0 - t.classification.score, 0.7) * h)
              .attr('width', 15)
              .attr('height', '100%')
              .attr('fill', colormap(t.classification.stance));

          bar.append('line')
              .attr('stroke', 'black')
              .attr('stroke-dasharray', '1, 1')
              .attr('x1', 0)
              .attr('y1', 0.7 * h)
              .attr('x2', 15)
              .attr('y2', 0.7 * h);
        })
        .on('dblclick', function(d) {
          selected = (this == selected ? null : this);
          updateSelection();
        })
  });

  function updateSelection() {
    if (selected) {
      join.classed('selected', false);
      join.classed('unselected', true);
      d3.select(selected).classed('unselected', false);
      d3.select(selected).classed('selected', true);
    }
    else {
      join.classed('unselected', false);
      join.classed('selected', false);
    }
  }

}

function getClassification(fullClassif) {
  // Clean it up a little bit first
  if (typeof(fullClassif) == 'string')
    fullClassif = JSON.parse(fullClassif);
  fullClassif = fullClassif[0];

  // Interpret classification results (as best as possible)
  let classification = 'None';
  let score = 0;
  for (let k of Object.keys(fullClassif)) {
    let v = fullClassif[k];
    if (k != 'sentence_text' && v[0] != 'no') {
      let tmp_score = 0
      if (v[2][0][0] != 'no')
          tmp_score = parseFloat(v[2][0][1]);
      else
          tmp_score = parseFloat(v[2][1][1]);
      if (tmp_score > score) {
        score = tmp_score;
        classification = v[0];
      }
    }
  }

  return { stance: classification, score: score };
}
