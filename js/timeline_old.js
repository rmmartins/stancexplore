// ---------- Draw Timeline ----------

function timelineOld2(data, colormap) {
  // all classifications per day
  let timeseries = data.reduce((prev, curr) => {
    let classif = prev.get(curr['classification']) || new Map();
    let count = classif.get(curr['date']) || 0;
    classif.set(curr['date'], count + 1);
    prev.set(curr['classification'], classif);
    return prev;
  }, new Map());

  let dailySums = countByKey(data, 'date');

  for (day of timeseries) {
    let d = timeseries.get(day[0]);
    d.set('2016-05-05', 0);
    timeseries.set(day[0], new Map([...d.entries()].sort()))
  }

  let timeFormat = d3.time.format("%Y-%m-%d");

  data = [...timeseries].map(([key, value]) => {
    return {
      key: key,
      values: [...value].map(([k, v]) => {
        return {
          date: timeFormat.parse(k),
          count: +v
        }
      })
    }
  });

  console.log(data);
  return;

  let width = d3.select('.container').node().getBoundingClientRect().width,
      height = 170,
      padding_top = 20,
      padding_left = 50,
      padding_bottom = 20,
      padding_right = 20;

  let svg = d3.select('#timeline')
      .attr('width', width)
      .attr('height', height);

  let x = d3.time.scale()
    .domain(d3.extent(data[0].values, d => d.date))
    .rangeRound([0, width - padding_left - padding_right  ]);

  let y = d3.scale.linear()
    .domain([0, d3.max(dailySums, d => d.value)]).nice()
    .rangeRound([height - padding_bottom, 0]);

  let y0 = Array(31).fill(0), y1 = [];

  let area = d3.svg.area()
    .x(d => x(d.date))
    .y0((d,i) => y(y0[i]))
    .y1((d,i) => y(y1[i]))
    .interpolate('cardinal');

  for (let i = 0; i < data.length; ++i) {
    let layer = data[i];
    y1 = layer.values.map((d,i) => d.count + y0[i]);
    svg.append('g')
        .attr('transform', `translate(${padding_left}, 0)`)
      .append("path")
        .datum(layer.values)
        .attr("fill", colormap(layer.key))
        .attr("d", area);
    y0 = y1.slice();
  }

  let xAxis = d3.svg.axis()
      .scale(x)
      .orient('bottom');

  let yAxis = d3.svg.axis()
      .scale(y)
      .orient('left')
      .ticks(5);

  svg.append('g')
      .attr('class', 'x axis')
      .attr('transform', `translate(${padding_left}, ${height - padding_bottom})`)
    .call(xAxis);

  svg.append('g')
      .attr('class', 'y axis')
      .attr('transform', `translate(${padding_left},0)`)
    .call(yAxis);

}


function timelineOld(data, colormap) {

  let cWidth = d3.select('.container').node().getBoundingClientRect().width;
  let padding_top = 20;
  let padding_left = 50;
  let padding_bottom = 20;
  let padding_right = 20;
  let tlHeight = 170;
  let tlWidth = cWidth - padding_right;

  let timeseries = countByKey(data, 'date');
  let timeseries_values = timeseries.map(d => d.value);

  // in-place assignment of categories (d => d.cluster)
  classify(timeseries, data, 'date');

  let timelineSvg = d3.select('#timeline')
      .attr('width', tlWidth)
      .attr('height', tlHeight)
    .append('g')
      .attr('transform', `translate(${padding_left}, ${padding_top})`);

  let xScale = d3.scale.ordinal()
      .domain(timeseries.map(d => +(d.key.split('-')[2])))
      .rangeRoundBands([0, tlWidth], 0.1);

  let maxHeight = tlHeight - padding_top - padding_bottom;

  let yScale = d3.scale.linear()
      .domain([0, d3.max(timeseries_values)])
      .range([maxHeight, 0]);

  let xAxis = d3.svg.axis()
      .scale(xScale)
      .orient('bottom');

  let yAxis = d3.svg.axis()
      .scale(yScale)
      .orient('left')
      .ticks(5);

  // let colormap = d3.scale.linear()
  //     .domain(d3.extent(timeseries_values))
  //     .range(['#ece7f2','#2b8cbe']);

  timelineSvg.selectAll('rect')
      .data(timeseries)
    .enter().append('rect')
      .attr('x', d => xScale(+(d.key.split('-')[2])))
      .attr('y', d => yScale(d.value))
      .attr('width', xScale.rangeBand())
      .attr('height', d => maxHeight - yScale(d.value))
      .style('fill', d => colormap(d.cluster));

    timelineSvg.append('g')
        .attr('class', 'x axis')
        .attr('transform', `translate(0, ${maxHeight})`)
      .call(xAxis);

    timelineSvg.append('g')
        .attr('class', 'y axis')
      .call(yAxis);
}
