
let selected = null, div;

function users(full_data) {
  d3.json('data/users.json', function(error, data) {
    // In-place assignment of categories
    classify(data, full_data, 'user_id');

    let width = 270, height = width;

    let radius = 5, margin = radius;
    let svg = d3.select('#users')
      .append('svg')
        .attr('width', width)
        .attr('height', height);

    // Get the maximum and minimum of both axes, to ensure a square domain
    let xExtent = d3.extent(data, d => d['coords'][0]);
    let yExtent = d3.extent(data, d => d['coords'][1]);
    let domain = d3.extent([...xExtent, ...yExtent]);
    // When width == height the range is also square
    let x = d3.scale.linear()
      .domain(domain)
      .range([0, width - 2 * margin]);
    let y = d3.scale.linear()
      .domain(domain)
      .range([height - 2 * margin, 0]);

    // let xAxis = d3.svg.axis()
    //   .scale(x)
    //   .orient('bottom');

    // let yAxis = d3.svg.axis()
    //   .scale(y)
    //   .orient('left');

    // Center the scatterplot
    let hDiff = width - x(xExtent[1]) + x(xExtent[0]);
    let vDiff = height - y(yExtent[0]) + y(yExtent[1]);
    let g = svg.append('g')
        .attr('transform', `translate(${hDiff / 2}, ${vDiff / 2})`);

    // g.append('g')
    //     .attr('transform', `translate(0, ${y(0)})`)
    //     .attr('class', 'axis')
    //     .call(xAxis);

    // g.append('g')
    //     .attr('transform', `translate(${x(0)}, 0)`)
    //     .attr('class', 'axis')
    //     .call(yAxis);

    data.sort((a,b) => a['count'] - b['count']);

    div = d3.select("#users").append("div")
      .attr("class", "user-tooltip")
      .style("display", "none");

    let svgCoords = svg.node().getBoundingClientRect();

    let radScale = d3.scale.linear()
      .domain(d3.extent(data.map(d => d['count'])))
      .range([4, radius * 3]);

    data.sort(d => d['count']);

    let circles = g.selectAll('circle')
        .data(data)
      .enter().append('circle')
        .attr('cx', d => x(d['coords'][0]))
        .attr('cy', d => y(d['coords'][1]))
        .attr('r', d => radScale(d['count']))
        .attr('fill', d => colormap(d.cluster))
        .on('click', function(d) {
          // Remove selection first
          d3.select(selected).classed('selected', false);
          selected = this;
          circles.style('opacity', '0.25');
          d3.select(this)            
            .style('opacity', '1.0')
            .classed('selected', true);
          showTooltip(d);
          // Stop propagation or else the SVG will clear the selection instantly
          d3.event.stopPropagation();
          // Testing
          userDim.filter(d.user_id);
        })
        .on('mouseenter', function(d) {
          if (selected == null) {
            showTooltip(d);
          }
        })
        .on('mouseout', function(d) {
          if (selected == null) {
            div.style("display", "none");
          }
        });

    // Clear selection
    svg.on('click', function() {
      d3.select(selected).classed('selected', false);
      selected = null;
      circles.style('opacity', '1.0');
      div.style("display", "none");
      userDim.filterAll();
    });

    // function getCoords(div, object) {
    //   let objBounds = object.getBoundingClientRect();
    //   let divBounds = div.node().getBoundingClientRect();
    //   return [
    //       objBounds.left - svgCoords.left - divBounds.width / 2 + objBounds.width / 2, 
    //       objBounds.top - svgCoords.top - divBounds.height - 5 
    //     ];
    // }

  })
}

function showTooltip(d) {
  // This must come before getCoords because this sets the div's width
  // and height to fit the content
  div
    .html(`<span>${d['user_id']}</span><br>${d['count']} tweets`)
    .style("display", "inline-block");
  // Must recalculate because the user may have resized the window
  // svgCoords = svg.node().getBoundingClientRect();
  // let coords = getCoords(div, this);
  let coords = [5, 5];          
  div
    .style("right", coords[0] + "px")
    .style("bottom",  coords[1] + "px");
}
