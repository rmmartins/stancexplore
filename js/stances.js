function stances(parentId) {
  let niceNames = {
    None: 'Neutral',
    need_requirement: 'Necessity',
    source_of_knowledge: 'Source of Knowledge',
    uncertainty: 'Uncertainty',
    hypotheticals: 'Hypotheticality',
    agreement_and_disagreement: 'Agreement/Disagreement',
    volition: 'Volition',
    prediction: 'Prediction',
    tact_and_rudeness: 'Tact/Rudeness',
    certainty: 'Certainty',
    concession_and_contrariness: 'Contrariety'
  };

  let svg, x, scale, items, selected = [];

  let width = 300, height = 220;

  svg = d3.select(parentId).append('svg')
    .attr('width', width)
    .attr('height', height)
    .on('click', function(d) {
      if (d3.event.metaKey || d3.event.ctrlKey) {
        // clear selection
        selected = [];        
      }
      updateSelection();
    });

  scale = d3.scale.ordinal()
    .domain(d3.range(categories.length))
    .rangeBands([0, height], .1);

  x = d3.scale.linear()
    .domain([0, d3.max(categories.map(d => d.value))])
    .range([30, width / 2 - 20]);

  items = svg.selectAll('.legend_item')
      .data(categories)
    .enter().append('g')
      .attr("class", "legend_item")
      .on('click', function(d) {
        // command or ctrl+click clears selection (by propagation to parent node)
        if (d3.event.metaKey)
          return;
        // Ignore click if value == 0
        if (d.value == 0) {
          d3.event.stopPropagation();
          return;
        }
        // Shift-click = toggle selection
        // Normal click = exclusive selection
        if (d3.event.shiftKey) {
          if (selected.includes(this))
            selected.splice(selected.indexOf(this), 1);
          else
            selected.push(this);
        } else {
          if (selected.length == 1 && selected[0] == this) {
            // Don't do anything; selection didn't change
            d3.event.stopPropagation();
          } else {
            selected = [this];
          }
        }
      });

  // Initial selection = all stances
  selected = items[0].slice();

  items.append('rect')
      .attr("class", "background")
      .attr('x', 5)
      .attr('y', function(d,i) { return scale(i) - 0.5; })
      .attr('width', width - 10)
      .attr('height', scale.rangeBand() + 1);

  items.append('text')
      .attr('class', 'name')
      .attr('x', width / 2 - 3)
      .attr('y', function(d,i) { return scale(i) + scale.rangeBand() / 2; })
      .text(function(d) { return niceNames[d.key]; });

  items.append('rect')
    .attr("class", "legend")
    .attr('x', width / 2 + 5)
    .attr('y', function(d,i) { return scale(i) + 1; })
    .attr('width', d => x(d.value))
    .attr('height', scale.rangeBand() - 2)
    .style('fill', function(d) { return colormap(d.key); });

  items.append('text')
    .attr('class', 'value')
    .attr('x', width / 2 + 10)
    .attr('y', function(d,i) { return scale(i) + scale.rangeBand() / 2; })
    .text(d => d.value);

  function updateSelection() {
    // A selection can never be empty
    if (selected.length == 0) {
      selected = items[0].slice();
    }
    svg.selectAll('.legend_item').style('opacity', 0.25);
    for (let sel of selected) {
      d3.select(sel).style('opacity', 1);
    }
    let keys = selected.map(x => x.__data__.key);
    categDim.filterFunction(d => {
      return keys.includes(d);
    }); 
  }

  return function() {
    // Re-group with filters
    categories = categDim.group().all();
    // Update the scale so that the bars aren't too small
    x.domain([0, d3.max(categories.map(d => d.value))]);
    // Update data
    items.data(categories);
    // There is no enter or exit selection, only update
    items.selectAll('.value')
        .text(d => d.value);
    items.selectAll('.legend')
      .transition()
        .attr('width', d => x(d.value));
  }
}
