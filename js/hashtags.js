function hashtags(parentId, selected = []) {
  let items;

  function sort() {
    // Sort in descending order, but the selected row is always on top
    items.sort((a, b) => {
      let aSel = selected.includes(a.key);
      let bSel = selected.includes(b.key);

      if ((aSel && bSel) || selected.length == 0)
        return d3.descending(a.value, b.value);

      if (aSel && !bSel)
        return -1;

      if (!aSel && bSel)
        return 1;
        
      let selKey = selected[0];
      // The rest is sorted according to their similarity with the selected one
      // https://en.wikipedia.org/wiki/S%C3%B8rensen%E2%80%93Dice_coefficient
      let aValue = stringSimilarity.compareTwoStrings(a.key, selKey);
      let bValue = stringSimilarity.compareTwoStrings(b.key, selKey);
      return d3.descending(aValue, bValue);
    });
  }

  d3.select(parentId)
      .on('click', d => {
        if (d3.event.metaKey || d3.event.ctrlKey) {
          // clear selection
          while (selected.length > 0) selected.splice(0, 1);
        }
        // An empty selection is a special case
        if (selected.length == 0) {
          htDim.filterAll();
        } else {
          htDim.filterFunction(tag => {
            return selected.includes(tag);
          });
        }
      });
  
  let draw = function() {
    // Get top 100 hashtags (with or without filters)
    let data = htDim.group().top(100);

    // Attach hashtags to table (as items)
    items = d3.select(parentId + ' > tbody').selectAll('tr')
        .data(data, d => d.key);

    // Remove hashtags that don't match the filter anymore
    items.exit().remove();

    // Create new items for entering hashtags
    items.enter().append('tr')
        .attr('class', 'item')
        .on('mouseover', function(d) {
          let row = d3.select(this);
          if (!row.classed('ht-selected'))
            row.classed('ht-hover', true);
        })
        .on('mouseout', function(d) {
          d3.select(this).classed('ht-hover', false);
        })
        .on('click', function(d) {
          // command or ctrl+click clears selection (by propagation to parent node)
          if (d3.event.metaKey || d3.event.ctrlKey)
            return;
          // Shift-click = toggle selection
          // Normal click = exclusive selection
          if (d3.event.shiftKey) {
            if (selected.includes(d.key))
              selected.splice(selected.indexOf(d.key), 1);
            else
              selected.push(d.key);
          } else {
            if (selected.length == 1 && selected[0] == d.key) {
              // Don't do anything; selection didn't change
              d3.event.stopPropagation();
            } else {
              while (selected.length > 0) selected.splice(0, 1);
              selected.push(d.key);
            }
          }
        })
        .each(function(d) {
          d3.select(this).append('td').append('span')
              .classed('key', true)
              .text(d => d.key);
          d3.select(this).append('td').append('span')
              .classed('value', true);
        });

    // Update all hashtags with new value
    items.select('.value').text(d => d.value);

    items.each(function(d) {
      if (selected.includes(d.key)) {
        d3.select(this)
          .classed('ht-hover', false)
          .classed('ht-selected', true);
      } else {
        d3.select(this)
          .classed('ht-selected', false);
      }
    });

    sort();
  }

  draw.selected = selected;

  return draw;
}
