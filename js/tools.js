function genColormap(data, range) {
  let e = d3.extent(data);
  let d = d3.range(e[0], e[1], (e[1]-e[0])/(range.length-1));
  return d3.scale.linear()
    .domain(d)
    .range(range);
}

function countByKey(data, key, asArray=true) {
  // first, convert data into a Map with reduce
  let counts = data.reduce((prev, curr) => {
    let count = prev.get(curr[key]) || 0;
    prev.set(curr[key], count + 1);
    return prev;
  }, new Map());

  // then, map your counts object back to an array
  if (asArray) {
    counts = [...counts].map(([key, value]) => {
      return {key, value}
    })
  }

  return counts;
}

function categoriesByKey(data, key) {
  return data.reduce((accum, item) => {
    let cat = item['classification'];
    let resItem = accum.get(item[key]) || new Map();
    let prevCount = resItem.get(cat) || 0;
    resItem.set(cat, prevCount + 1);
    accum.set(item[key], resItem);
    return accum;
  }, new Map());
}

// Classify each item of 'data' according to the most common category of its
// sub-elements, as found in full_data. The category is assigned in-place as
// d.cluster (and d.clusterCount).
function classify(data, full_data, key, categories) {
  let classifData = {};
  let keys = data.map(d => d.key || d[key]);
  full_data.filter(d => keys.indexOf(d[key]) > -1).forEach(d => {
    let classif = d['classification'];
    if (!categories || categories.length == 0 || categories.indexOf(classif) > -1) {
      let id = d[key];
      let item = classifData[id] || {};
      let count = item[classif] || 0;
      item[classif] = count + 1;
      classifData[id] = item;
    }
  });

  for (d of data) {
    let id = d.key || d[key];
    d.clusterCount = 0;
    if (classifData[id]) {
      if (categories && categories.length == 1) {
        d.cluster = categories[0];
        d.clusterCount = classifData[id][d.cluster] || 0;
      } else if (categories && categories.length == 2) {
        d.cluster = `${categories[0]}, ${categories[1]}`;
        let c0 = classifData[id][categories[0]] || 0;
        let c1 = classifData[id][categories[1]] || 0;
        d.clusterCount = (c1 - c0) / (c1 + c0);
      } else {
        Object.keys(classifData[id]).forEach(c => {
          if (c != 'None' && classifData[id][c] > d.clusterCount) {
            d.clusterCount = classifData[id][c];
            d.cluster = c;
          }
        });
        if (!d.hasOwnProperty('cluster')) {
          d.cluster = 'None';
        }
      }
    }
  }

}
